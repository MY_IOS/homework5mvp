
import UIKit
import Kingfisher

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var articleDesctriptionLable: UILabel!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setArticleInformation(article: Articles) {
        self.articleTitleLabel.text = article.title
        self.articleDesctriptionLable.text = article.description
        
        let url = URL(string: article.image ?? "DefaultImage")
        self.articleImageView.kf.setImage(with: url, placeholder: UIImage(named: "DefaultImage"))
    }
    
}
